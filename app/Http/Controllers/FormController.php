<?php

namespace App\Http\Controllers;

use App\Models\App;
use Michelf\Markdown;
use App\Models\Telegram;
use App\Helpers\IPHelper;
use App\Models\RequestForm;
use App\Http\Requests\StoreRequestForm;

class FormController extends Controller
{
    public function store(StoreRequestForm $request)
    {
        $data = $request->validated();
        $model = new RequestForm();
        $model->fill($data);
        $model->ip = IPHelper::get();

        if (!$model->save()) {
            return App::makeErrorResponse(500, [], 'Возникла ошибка');
        }

        $this->sendMailToManagers($model);
        $this->sendTelegram($model);

        return App::makeSuccessResponse([]);
    }

    protected function getMessage(RequestForm $model){
        $message = null;

        if($model->form_type === 'subscribe'){
            $message = "Новая заявка с формы *Подписка*\n".
                "E-mail: *{$model->email}*\n";
        }
        elseif($model->form_type === 'callback'){
            $message = "Новая заявка с формы *Перезвоните мне*\n".
                "Телефон: *{$model->phone}*\n";
        }
        elseif($model->form_type === 'booking'){
            $message = "Новая заявка с формы *Забронировать*\n".
                "Имя: *{$model->name}*\n".
                "ФИО ребенка: *{$model->fio_children}*\n".
                "Телефон: *{$model->phone}*\n".
                "E-mail: *{$model->email}*\n".
                "Заявка на (название смены): *{$model->claim_for}*\n".
                "Площадка (название площадки): *{$model->platform}*\n".
                "На дату (даты смены): *{$model->to_date}*";
        }

        return $message;
    }

    protected function sendTelegram(RequestForm $model){
        if (env('TG_ENABLED', false) != true) {
            return;
        }
        $chats = explode('|', env('TG_CHATS'));
        $message = $this->getMessage($model);
        if(!$message){
            return;
        }
        foreach ($chats as $chatId) {
            $telegramNotifier = new Telegram(
                env('TG_TOKEN'),
                $chatId
            );
            $telegramNotifier->send(urlencode($message));
        }
    }

    protected function sendMailToManagers(RequestForm $model)
    {
        $message = $this->getMessage($model);
        if(!$message){
            return;
        }
        $htmlMessage = Markdown::defaultTransform($message);
        $htmlMessage = implode('<br />', explode(PHP_EOL, $htmlMessage));
        $pageTitle = "Новая заявка на бронирование с сайта \"4-u.ru\"";
        $emails = explode('|', env('MAIL_MANAGERS'));
        $headers = 'From: 4-u <noreply@4-u.ru>' . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=utf-8\r\n";
        $headers .= "Content-Transfer-Encoding: 8bit\r\n\r\n";
        if(!empty($emails)){
            foreach ($emails as $email) {
                mail($email, $pageTitle, $htmlMessage, $headers);
            }
        }
    }
}
