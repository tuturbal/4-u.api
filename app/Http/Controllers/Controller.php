<?php

namespace App\Http\Controllers;

use App\Models\App;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function makeSuccessResponse($data = [])
    {
        return App::makeSuccessResponse($data);
    }

    protected function makeErrorResponse($code = 500, $data = [], $msg = '')
    {
        return App::makeErrorResponse($code, $data, $msg);
    }
}
