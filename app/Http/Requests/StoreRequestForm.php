<?php

namespace App\Http\Requests;

class StoreRequestForm extends BaseRequest
{
    public function rules()
    {
        return [
            'form_type' => 'required|max:255|in:callback,subscribe,booking',
            'phone' => 'required|digits_between:10,16',
            'name' => 'max:255',
            'fio_children' => 'max:255',
            'email' => 'max:255',
            'claim_for' => 'max:255',
            'platform' => 'max:255',
            'to_date' => 'date|nullable',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Заполните обязательные поля',
            'phone.digits_between' => 'Введен некорректный номер телефона',
            'form_type.in' => 'Введено некорректное значение типа формы',
            //'phone.unique' => 'Заявка с данным телефоном уже существует',
        ];
    }
}
