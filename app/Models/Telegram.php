<?php

namespace App\Models;

use GuzzleHttp\Client;
use App\Interfaces\MessengerNotifier;

class Telegram implements MessengerNotifier
{
    protected $client;
    protected $token, $chatID;
    protected $parseMode = 'MARKDOWN';
    protected $disableWebPagePreview = true;
    protected $proxy;

    /**
     * Telegram constructor.
     * @param $token
     * @param $chatID
     */
    public function __construct($token, $chatID)
    {
        $this->proxy = config('telegram.proxy');
        $this->proxy = [
            'schema' => env('TG_PROXY_SCHEMA'),
            'host' => env('TG_PROXY_HOST'),
            'port' => env('TG_PROXY_PORT'),
            'user' => env('TG_PROXY_USER'),
            'pass' => env('TG_PROXY_PASS'),
        ];
        $this->chatID = $chatID;
        $this->token = $token;

        $proxyOptions = $this->proxy['schema'].'://'.$this->proxy['user'].':'.$this->proxy['pass'].'@'.$this->proxy['host'].':'.$this->proxy['port'];

        $this->client = new Client([
            "proxy" => $proxyOptions,
        ]);
    }

    /**
     * Послать сообщение
     * @param $message
     * @return bool
     */
    public function send($message) : bool
    {
        try {
            $url = "https://api.telegram.org/bot$this->token/sendMessage?text=$message&chat_id=$this->chatID&parse_mode=$this->parseMode";
            $result = $this->client->request('GET', $url);
        } catch (\Exception $e) {
            return false;
        }
        if ($result->getStatusCode() !== 200) {
            return false;
        }

        return true;
    }

    public function getUpdates(){
        $url = "https://api.telegram.org/bot$this->token/getUpdates?offset=4";
        try {
            $response = $this->client->get($url);
            if ($response->getBody()) {
                echo $response->getBody();
                // JSON string: { ... }
            }

            die;
        } catch (\Exception $e) {
            var_dump($e);
        }
    }
}