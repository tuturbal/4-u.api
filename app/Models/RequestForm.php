<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class RequestForm
 * @package App\Models
 * @property string $form_type Тип формы
 * @property string $name Имя
 * @property string $fio_children ФИО ребенка
 * @property string $phone Телефон
 * @property string $email E-mail
 * @property string $claim_for Заявка на: (название смены)
 * @property string $platform Площадка: (название площадки)
 * @property string $to_date На дату: (даты смены)
 */
class RequestForm extends Model
{
    protected $table = 'request_form';
    protected $fillable = ['form_type', 'name', 'fio_children', 'phone', 'email', 'claim_for', 'platform', 'to_date'];
}
