<?php

namespace App\Interfaces;

interface MessengerNotifier
{
    public function send($message) : bool;
}